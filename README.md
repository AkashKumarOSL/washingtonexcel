Follow the steps below to test files.

1. Run `composer install`
2. Put the excel sheet in `Washington/excel`
3. Rename file name to `data.csv` as of now.
4. Exceute file `Washington/washington/Washington.php`
5. New created excel files will be stored in `Washington/NewExcelData`
