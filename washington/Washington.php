<?php 

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;

// File name with location.
$excelFileName = '../excel/data.csv';

$spreadsheet = IOFactory::load($excelFileName);

$sheetData = $spreadsheet->getActiveSheet();

// Highest Row of excel sheet.
$highest_row = $sheetData->getHighestRow();
// Highest Column of excel sheet.
$highest_col = $sheetData->getHighestColumn();

for ($row=1; $row <= $highest_row; $row++) {
  if($sheetData->getCell('A'.$row)->getValue()[0] == '/') {
    $index[] = $row;
  }
}  

foreach(range('A',$highest_col) as $column) {
  foreach ($index as $value)  {
    $column_data[$column][$value] = $sheetData->getCell($column.$value)->getValue();
  }
}

// Total sum of Pageviews in excel sheet.
$sum = 0;
foreach ($column_data['B'] as $key => $value) {
  $value = str_replace( ',', '', $value);
  $sorted_data[$key] = $value;
  $sum = $sum + $value;
}
arsort($sorted_data);
 
// 90% of total sum of columnB(Pageviews) values.
$per = round(($sum*90)/100);

$addSum = 0;
foreach ($sorted_data as $key => $value) {
  if($addSum <= $per) {
    $addSum+= $value;
    $new_columnB[$key] = $value; 
  }
}
// Remove query parameters from urls.
foreach ($column_data['A'] as $key => $value) {
  $url = strtok($value, '?');
  $page[$key] = $url;
}

//
$new_array_str = $new_columnB;
// Sorted columnA(Page) according to pageviews (Desc->Asc). 
foreach ($new_array_str as $key => $value) {
  $sort_str[$key] = $page[$key];
}

// First excel sheet (5 Mil Page Visits).
$data[] = [
      'Page',
      'Pageviews',
      'Unique Pageviews',
      'Avg. Time on Page',
      'Entrances',
      'Bounce Rate',
      '% Exit',
      'Page Value'
    ];

foreach ($sort_str as $key => $value) {
  $data[] = [
    $page[$key],
    $column_data['B'][$key],
    $column_data['C'][$key],
    $column_data['D'][$key],
    $column_data['E'][$key],
    $column_data['F'][$key],
    $column_data['G'][$key],
    $column_data['H'][$key]
  ]; 
}
$fp = fopen('../NewExcelData/file1.csv', 'w');
foreach ($data as $key => $fields) {
  fputcsv($fp, $fields);
}
fclose($fp);
echo "Created first excel sheet (5 Mil Page Visits)".PHP_EOL;

// Second excel sheet (Query stripped and merged).
$page = $sort_str;
foreach ($page as $key => $value) {
  $page1[$key] = $value;  
}
if(!empty($page1)) {
  asort($page1);
  $my_arr = $page1;
  $dups = grouping_same_url_values($my_arr);
  $new_excel = $dups;
  createExcelSheet($column_data, $new_excel, $page1, 'file2');
  echo "Created second excel sheet (Query stripped and merged)".PHP_EOL;
}  

// Explode page urls.
$page = $sort_str;
foreach ($page as $key => $value) {
  $sep = explode('/', $value);
  if(empty($sep[1])) {
    $page2[$key] = '/';
  }
  else {
    $page2[$key] = $sep[1];
  }
  if(!empty($sep[2])) {
    $page3[$key] = $sep[2];
  }
  if(!empty($sep[3])) {
    $page4[$key] = $sep[3];
  }
  if(!empty($sep[4])) {
    $page5[$key] = $sep[4];
  }
}
// Third excel sheet (L1 Categories to Preserve).
if(!empty($page2)) {
  asort($page2);
  $my_arr = $page2;
  $dups = grouping_same_url_values($my_arr);
  $new_excel = $dups;
  createExcelSheet($column_data, $new_excel, $page2, 'file3');
  echo "Created third excel sheet (L1 Categories to Preserve)".PHP_EOL;
}

// Fourth excel sheet (L2 Categories to Preserve).
if(!empty($page3)) {
  asort($page3);
  $my_arr = $page3;
  $dups = grouping_same_url_values($my_arr);
  $new_excel = $dups;
  createExcelSheet($column_data, $new_excel, $page3, 'file4');
  echo "Created Fourth excel sheet (L2 Categories to Preserve)".PHP_EOL;
}  

// Fifth excel sheet (L3 Categories to Preserve).
if(!empty($page4)) {
  asort($page4);
  $my_arr = $page4;
  $dups = grouping_same_url_values($my_arr);
  $new_excel = $dups;
  createExcelSheet($column_data, $new_excel, $page4, 'file5');
  echo "Created Fifth excel sheet (L3 Categories to Preserve)".PHP_EOL;
}  

// Sixth excel sheet (L4 Categories to Preserve).
if(!empty($page5)) {
  asort($page5);
  $my_arr = $page5;
  $dups = grouping_same_url_values($my_arr);
  $new_excel = $dups;
  createExcelSheet($column_data, $new_excel, $page5, 'file6');
  echo "Created sixth excel sheet (L4 Categories to Preserve)".PHP_EOL;
}

// Grouping same url values.
function grouping_same_url_values($my_arr) {
  $group = array();
  foreach ($my_arr as $key => $val) {
    if (isset($group[$val])) {
       $group[$val][] = $key;
    } 
    else {
       $group[$val] = array($key);
    } 
  }
  return $group;
}

// Prepares and create excel sheet.
function createExcelSheet($column_data, $new_excel, $page, $file_name) {
  $new_data[] = [
        'Page',
        'Pageviews',
        'Unique Pageviews',
        'Avg. Time on Page',
        'Bounce Rate',
        '% Exit'
      ]; 
  foreach ($new_excel as $key => $value) {
    $pageviews = 0;
    $unique_pageview = 0;
    $sum_avg_time = 0;
    $bounce_rate = 0;
    $exit = 0;
    foreach ($value as $k => $v) {
      // Calculation.
      $column_data['B'][$v] = str_replace( ',', '', $column_data['B'][$v]);
      $pageviews += $column_data['B'][$v];
      $column_data['C'][$v] = str_replace( ',', '', $column_data['C'][$v]);
      $unique_pageview += $column_data['C'][$v];
      // Weighted average for average_time.
      list($h, $m, $s) = explode(':', $column_data['D'][$v]);
      $column_data['D'][$v] = ($h * 3600) + ($m * 60) + $s;
      $sum_avg_time += $column_data['B'][$v]*$column_data['D'][$v];
      // Weighted average for Bounus Rate.
      $bounce_rate += ($column_data['B'][$v]*str_replace('%', '', $column_data['F'][$v]))/100;
      // Weighted average for Exit.
      $exit += ($column_data['B'][$v]*str_replace('%', '', $column_data['G'][$v]))/100;
    }
    $avg_time = round($sum_avg_time/$pageviews);
    $avg_time = sprintf('%02d:%02d:%02d', ($avg_time/ 3600), ($avg_time/ 60 % 60), $avg_time%60);
    $bounce_rate = number_format(($bounce_rate/$pageviews)*100, 2)."%";
    $exit = number_format(($exit/$pageviews)*100, 2)."%";
    $new_data[] = [
      $page[$v],
      $pageviews,
      $unique_pageview,
      $avg_time,
      $bounce_rate,
      $exit
    ];
  }
  $fp = fopen('../NewExcelData/'.$file_name.'.csv', 'w');
  foreach ($new_data as $key => $fields) {
    fputcsv($fp, $fields);
  }
  fclose($fp);
}