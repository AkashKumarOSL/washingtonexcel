<?php 

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;

$excelFileName = '../excel/data.csv';
$spreadsheet = IOFactory::load($excelFileName);
$sheetData = $spreadsheet->getActiveSheet();
// Highest Row of excel sheet.
$highest_row_source = $sheetData->getHighestRow();

// Store data from excel sheet in array.
$data = array(1,$spreadsheet->getActiveSheet() 
            ->toArray(null,true,true,true));

error_reporting(E_ERROR | E_PARSE);

$file_flag = 0;
// Check if NewData.csv exits.
if(file_exists('../NewExcelData/NewData.csv')) {
  $spreadsheet_dest = IOFactory::load('../NewExcelData/NewData.csv');
	$sheetData_dest = $spreadsheet_dest->getActiveSheet();
	// Highest Row of excel sheet.
	$highest_row = $sheetData_dest->getHighestRow();
	$file_flag = 1;
}

// Webpage scraping.
foreach ($data as $key => $row) {
  // If destination file exits.
	if($file_flag == 1) {
    $row_start = $highest_row + 1;
	}
	else{
		$row_start = 1;
	}
	// Traverse every row of excel file.
  for($i = $row_start; $i <= sizeof($row); $i++) {
  	$url_flag = 0;
  	if($data[$key][$i]['A'][0] == '/') {
  		$url_flag = 1;
      $url = 'https://washington.org';
  		$urls = $url.$data[$key][$i]['A'];
  		echo "Count ".$i.PHP_EOL;
  		echo "Srcaping data from ".$urls.PHP_EOL;
  	  $str = file_get_contents($urls);
			// Gets Webpage Title
			if(strlen($str)>0)
			{
			  $str = trim(preg_replace('/\s+/', ' ', $str));
			  preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title);
			  $title=$title[1];
			}
			echo "Title:- ".$title.PHP_EOL;

			// Gets Webpage H1 tag
			$DOM = new DOMDocument;
			$DOM->loadHTML($str);
		  //get all H1
		  $items = $DOM->getElementsByTagName('h1');
		  //display all H1 text
		  for ($j = 0; $j < $items->length; $j++) {
		  	$h1[] = $items->item($j)->nodeValue;
		  }
		  if(sizeof($h1) > 1) {
			  echo "H1 tag:- ".serialize($h1).PHP_EOL;
			  $h1_tag = serialize($h1);
			}
			else {
				echo "H1 tag:- ".$h1[0].PHP_EOL;
			  $h1_tag = $h1[0];
			}  
		  echo "H1 tag count:- ".sizeof($h1).PHP_EOL;
		  $h1_count = sizeof($h1);
		  $h1 = [];

		  // Gets Webpage Description
			$url = parse_url($urls);
			$tags = get_meta_tags($url['scheme'].'://'.$url['host'].'/'.$url['path']);
			$description=$tags['description'];
			echo "Description:- ".$description.PHP_EOL;
  	}
  	if($url_flag == 1) {
	  	$data_row[$i] = [
				$data[$key][$i]['A'],
				$data[$key][$i]['B'],
				$data[$key][$i]['C'],
				$data[$key][$i]['D'],
				$data[$key][$i]['E'],
				$data[$key][$i]['F'],
				$data[$key][$i]['G'],
				$data[$key][$i]['H'],
				$title,
				$h1_tag,
				$h1_count,
				$description
			];
		}
		else {
			$data_row[$i] = [
				$data[$key][$i]['A'],
				$data[$key][$i]['B'],
				$data[$key][$i]['C'],
				$data[$key][$i]['D'],
				$data[$key][$i]['E'],
				$data[$key][$i]['F'],
				$data[$key][$i]['G'],
				$data[$key][$i]['H'],
			];
		}
		// Saves data in destionation excel file after every 10 iterations.			
  	if(($i%10) == 0) {
      // Create or append excel file
			$fp = fopen('../NewExcelData/NewData.csv', 'a+');
			foreach ($data_row as $row_key => $row_value) {
				fputcsv($fp, $row_value);
			}
			fclose($fp);
  	  $data_row = [];
  	}
  	if($highest_row_source-$i < 10) {
  		// Create or append excel file
			$fp = fopen('../NewExcelData/NewData.csv', 'a+');
			foreach ($data_row as $row_key => $row_value) {
				fputcsv($fp, $row_value);
			}
			fclose($fp);
  	  $data_row = [];
  	}
  }
}
